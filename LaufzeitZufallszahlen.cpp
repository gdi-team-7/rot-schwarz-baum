#include "Tree.h"
#include "Tree.cpp"
#include <ctime>
#include <iostream>
using namespace std;

int main() {
    Tree myTree;
    struct infoType myInfo;
    
    //Start
    clock_t start = clock();

    // 10.000.000 Inserts
    for (int i = 0; i < 60000000; ++i) {
        int randomKey = rand() % 60000000;
        myInfo.Information[0] = 'A' + i;
        myTree.Tree_Insert(randomKey, &myInfo);
    }
    //Ende
    clock_t end = clock();
    
    // Gesamtlaufzeit
    double duration = (end - start) / (double)CLOCKS_PER_SEC;
    cout << "Laufzeit fuer 10.000.000 Inserts: " << duration << " Sekunden" << endl;

    return 0;
}
