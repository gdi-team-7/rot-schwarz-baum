#include "Tree.h"
#include "Tree.cpp"
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

using namespace std;

void insertFromDataFile(const string& filename, Tree& myTree, int& nodeCount) {
    ifstream dataFile(filename);
    if (!dataFile.is_open()) {
        cerr << "Fehler beim Öffnen der Datei: " << filename << endl;
        return;
    }

    string line;
    while (getline(dataFile, line)) {
        int id = stoi(line);
        
        struct infoType myInfo;
        myInfo.Information[0] = 'A' + nodeCount % 26;
        myTree.Tree_Insert(id, &myInfo);
        ++nodeCount;
    }

    dataFile.close();
}

int main() {
    Tree myTree;
    int nodeCount = 0;

    clock_t start = clock();
    insertFromDataFile("bremen-id.txt", myTree, nodeCount);
    clock_t end = clock();

    double duration = (end - start) / static_cast<double>(CLOCKS_PER_SEC);
    cout << "Laufzeit fuer Inserts basierend auf Echtdaten: " << duration << " Sekunden" << endl;
    cout << "Anzahl der eingefuegten Knoten: " << nodeCount << endl;

    return 0;
}
