#include "Tree.h"
#include <iostream>
using namespace std;


Tree::Tree() : Root(nullptr) {}

Tree::~Tree() {
    Tree_Destroy(Root);
}

void Tree::Tree_Insert(int key, struct infoType* info) {
    Node* z = new Node(key, info);
    Node* y = nullptr;
    Node* x = Root;

    if (Tree_Search(key) != nullptr) {
       // cout << "Schluessel " << key << " ist bereits vorhanden. Einfuegen abgebrochen." << endl;
        delete z;  // Speicher freigeben, da der Schlüssel bereits vorhanden ist
        return;
    }

    while (x != nullptr) {
        y = x;
        if (z->Key < x->Key)
            x = x->Left;
        else
            x = x->Right;
    }

    z->Up = y;
    if (y == nullptr)
        Root = z;
    else if (z->Key < y->Key)
        y->Left = z;
    else
        y->Right = z;
    z->Left = nullptr;
    z->Right = nullptr;
    z->IsRed = true;

    Tree_Insert_Fixup(z);
}

void Tree::Tree_Insert_Fixup(Node* z) {
    while (z->Up != nullptr && z->Up->IsRed) {
        if (z->Up == z->Up->Up->Left) {
            Node* y = z->Up->Up->Right;
            if (y != nullptr && y->IsRed) {
                z->Up->IsRed = false;
                y->IsRed = false;
                z->Up->Up->IsRed = true;
                z = z->Up->Up;
            } else {
                if (z == z->Up->Right) {
                    z = z->Up;
                    Left_Rotate(z);
                }
                z->Up->IsRed = false;
                z->Up->Up->IsRed = true;
                Right_Rotate(z->Up->Up);
            }
        } else {
            Node* y = z->Up->Up->Left;
            if (y != nullptr && y->IsRed) {
                z->Up->IsRed = false;
                y->IsRed = false;
                z->Up->Up->IsRed = true;
                z = z->Up->Up;
            } else {
                if (z == z->Up->Left) {
                    z = z->Up;
                    Right_Rotate(z);
                }
                z->Up->IsRed = false;
                z->Up->Up->IsRed = true;
                Left_Rotate(z->Up->Up);
            }
        }
    }
    Root->IsRed = false;
}

void Tree::Left_Rotate(Node* x) {
    Node* y = x->Right;
    x->Right = y->Left;
    if (y->Left != nullptr)
        y->Left->Up = x;
    y->Up = x->Up;
    if (x->Up == nullptr)
        Root = y;
    else if (x == x->Up->Left)
        x->Up->Left = y;
    else
        x->Up->Right = y;
    y->Left = x;
    x->Up = y;
}

void Tree::Right_Rotate(Node* x) {
    Node* y = x->Left;
    x->Left = y->Right;
    if (y->Right != nullptr)
        y->Right->Up = x;
    y->Up = x->Up;
    if (x->Up == nullptr)
        Root = y;
    else if (x == x->Up->Right)
        x->Up->Right = y;
    else
        x->Up->Left = y;
    y->Right = x;
    x->Up = y;
}

struct infoType* Tree::Tree_Search(int key) {
    Node* current = Root;
    while (current != nullptr && key != current->Key) {
        if (key < current->Key)
            current = current->Left;
        else
            current = current->Right;
    }

    if (current == nullptr) {
        //cout << "Gesuchter Schluessel nicht gefunden!" << endl;
        return nullptr; // Schlüssel nicht gefunden
    } else {
        //cout << "Schluessel " << key << " wurde gefunden" << endl;
        //cout << "Infoteil des Schluessels: " << current->Info->Information << endl;
        return current->Info; // Schlüssel gefunden, gib Info zurück
    }
}

void Tree::Tree_Print() {
    Tree_PrintHelper(Root, 0);
}

void Tree::Tree_PrintHelper(Node* root, int space) {
    if (root == nullptr)
        return;
    
    space += 5;

    Tree_PrintHelper(root->Right, space);

    std::cout << std::endl;
    for (int i = 5; i < space; i++)
        cout << " ";
    if(root->IsRed)
        cout << "[" << root->Key << "]" << " (RED)" << endl;
    else
        cout << "[" << root->Key << "]" << " (BLACK)" << endl;

    Tree_PrintHelper(root->Left, space);
}

void Tree::Tree_Delete(int key) {
    Node* z = Root;
    while (z != nullptr && z->Key != key) {
        if (key < z->Key)
            z = z->Left;
        else
            z = z->Right;
    }

    if (z == nullptr)
        return;

    Node* y = z;
    bool y_original_color = y->IsRed;
    Node* x;

    if (z->Left == nullptr) {
        x = z->Right;
        Transplant(z, z->Right);
    } else if (z->Right == nullptr) {
        x = z->Left;
        Transplant(z, z->Left);
    } else {
        y = Tree_Minimum(z->Right);
        y_original_color = y->IsRed;
        x = y->Right;
        if (y->Up == z) {
            if (x != nullptr)
                x->Up = y;
        } else {
            Transplant(y, y->Right);
            y->Right = z->Right;
            y->Right->Up = y;
        }
        Transplant(z, y);
        y->Left = z->Left;
        y->Left->Up = y;
        y->IsRed = z->IsRed;
    }
    if (y_original_color == false)
        Tree_Delete_Fixup(x);
    delete z;
}

void Tree::Tree_Delete_Fixup(Node* x) {
    while (x != Root && (x == nullptr || x->IsRed == false)) {
        if (x == x->Up->Left) {
            Node* w = x->Up->Right;
            if (w->IsRed) {
                w->IsRed = false;
                x->Up->IsRed = true;
                Left_Rotate(x->Up);
                w = x->Up->Right;
            }
            if ((w->Left == nullptr || w->Left->IsRed == false) && (w->Right == nullptr || w->Right->IsRed == false)) {
                w->IsRed = true;
                x = x->Up;
            } else {
                if (w->Right == nullptr || w->Right->IsRed == false) {
                    if (w->Left != nullptr)
                        w->Left->IsRed = false;
                    w->IsRed = true;
                    Right_Rotate(w);
                    w = x->Up->Right;
                }
                w->IsRed = x->Up->IsRed;
                x->Up->IsRed = false;
                if (w->Right != nullptr)
                    w->Right->IsRed = false;
                Left_Rotate(x->Up);
                x = Root;
            }
        } else {
            Node* w = x->Up->Left;
            if (w->IsRed) {
                w->IsRed = false;
                x->Up->IsRed = true;
                Right_Rotate(x->Up);
                w = x->Up->Left;
            }
            if ((w->Right == nullptr || w->Right->IsRed == false) && (w->Left == nullptr || w->Left->IsRed == false)) {
                w->IsRed = true;
                x = x->Up;
            } else {
                if (w->Left == nullptr || w->Left->IsRed == false) {
                    if (w->Right != nullptr)
                        w->Right->IsRed = false;
                    w->IsRed = true;
                    Left_Rotate(w);
                    w = x->Up->Left;
                }
                w->IsRed = x->Up->IsRed;
                x->Up->IsRed = false;
                if (w->Left != nullptr)
                    w->Left->IsRed = false;
                Right_Rotate(x->Up);
                x = Root;
            }
        }
    }
    if (x != nullptr)
        x->IsRed = false;
}

void Tree::Transplant(Node* u, Node* v) {
    if (u->Up == nullptr)
        Root = v;
    else if (u == u->Up->Left)
        u->Up->Left = v;
    else
        u->Up->Right = v;
    if (v != nullptr)
        v->Up = u->Up;
}

Tree::Node* Tree::Tree_Minimum(Node* x) {
    while (x->Left != nullptr)
        x = x->Left;
    return x;
}

void Tree::Tree_Destroy(Node* node) {
    if (node != nullptr) {
        Tree_Destroy(node->Left);
        Tree_Destroy(node->Right);
        delete node;
    }
}