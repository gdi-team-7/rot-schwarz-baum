#ifndef BINAERE_BAEUME_BIN_BAUM_H
#define BINAERE_BAEUME_BIN_BAUM_H

struct infoType {

     char Information[20];

};

class Tree {
public:
    Tree(); // Konstruktor
    ~Tree(); // Destruktor
    void Tree_Insert(int key, struct infoType* info); // Knoten in den Baum einfügen
    struct infoType* Tree_Search(int key); // Info-Teil vom Knoten mit "key" ausgeben
    void Tree_Print(); // Den Baum ausgeben (Wurzel steht links, also Baum um 90° gedreht)
    void Tree_Delete(int key); // Entferne Knoten mit "key"

private:
    struct Node {
        struct Node* Left;
        struct Node* Right;
        struct Node* Up;
        int Key;
        struct infoType* Info;
        bool IsRed; // Für Rot-Schwarz-Bäume

        Node(int key, struct infoType* info, bool isRed = true) : Left(nullptr), Right(nullptr), Up(nullptr), Key(key), Info(info), IsRed(isRed) {}
    };

    Node* Root;

    // Hilfsfunktionen für Rot-Schwarz-Bäume
    void Left_Rotate(Node* x);
    void Right_Rotate(Node* x);
    void Tree_Insert_Fixup(Node* z);
    void Tree_Delete_Fixup(Node* x);
    void Transplant(Node* u, Node* v);
    Node* Tree_Minimum(Node* x);
    void Tree_PrintHelper(Node* root, int space);
    void Tree_Destroy(Node* node);
};

#endif //BINAERE_BAEUME_BIN_BAUM_H