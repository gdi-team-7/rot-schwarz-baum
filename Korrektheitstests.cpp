#include "Tree.h"
#include "Tree.cpp"
#include <iostream>

using namespace std;

int main() {

    cout << "Anfang" << endl;
    cout << "--------------------------------------------" << endl;
    Tree myTree;

    
    //Versuch, einen schlüssel in einem leeren Baum zu suchen
    struct infoType* Ergebnis1 = myTree.Tree_Search(42);
    if(Ergebnis1 == nullptr){
       cout << "Schluessel wurde im leeren Baum nicht gefunden" << endl;
    }

    struct infoType info1 = {"Data1"};
    struct infoType info2 = {"Data2"};
    struct infoType info3 = {"Data3"};

    myTree.Tree_Insert(10, &info1);
    myTree.Tree_Insert(20, &info2);
    myTree.Tree_Insert(5, &info3);
    
    //Einfügen eines bereits vorhandenen Schlüssels
    myTree.Tree_Insert(10, &info1);


    //Suchen nach vorhandenen Schlüsseln
    struct infoType* Ergebnis2 = myTree.Tree_Search(10);
    if(Ergebnis1 == nullptr){
       cout << "Schluessel gefunden, Infoteil: " << Ergebnis2->Information << endl;
    }

    //Suche nach nicht vorhandenem Schlüsseln
    struct infoType* Ergebnis3 = myTree.Tree_Search(15);
    if(Ergebnis1 == nullptr){
       cout << "Schluessel wie gewollt nicht gefunden" << endl;
    }

    myTree.Tree_Print();

    cout << "--------------------------------------------" << endl;
    cout << "Ende" << endl;
    
    return 0;
}
